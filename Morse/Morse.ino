/**
 * Arduino sketch to send text via Morse code.
 */
#include "Morse.h"

/**
 * Global variables
 */
const unsigned long dotDuration = 300;        /**< Duration of a dot in ms. */
const unsigned long dashDuration = 600;       /**< Duration of a dash in ms. */
const unsigned long waitDuration = 5000;      /**< Duration between repetitions of the whole message. */

const byte pin1 = 12;                             /**< Arduino's pin attached to a led */
char text1[] = {'s','o','s'};                     /**< Array of chars containing the letters to send. */
int textLength1 = sizeof(text1)/sizeof(text1[0]); /**< Length of the array of chars. */

const byte pin2 = 9;                              /**< Arduino's pin attached to a led */
char text2[] = {'a','m','a','n','d','i','n','e'}; /**< Array of chars containing the letters to send. */
int textLength2 = sizeof(text2)/sizeof(text2[0]); /**< Length of the array of chars. */

Morse morse1(pin1, dotDuration, dashDuration, waitDuration, text1, textLength1); /** Instantiation of the Morse class. */
Morse morse2(pin2, dotDuration, dashDuration, waitDuration, text2, textLength2);

void setup()
{
}

void loop()
{
	morse1.run(); /**< Call of the Morse main function on each loop */
  morse2.run();
}
