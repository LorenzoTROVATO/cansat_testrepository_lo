#ifndef MORSE_H_
#define MORSE_H_

/**
 * Send an array of chars as Morse signs via a led attached to an arduino.
 * 
 */
class Morse {
private:
	int pinNumber;                    /**< Arduino's pin attached to a led. */
	unsigned long dotDuration;        /**< Duration of a dot in ms. */
	unsigned long dashDuration;       /**< Duration of a dash in ms. */
	unsigned long waitDuration;       /**< Duration between repetitions of the whole message. */
	char *text;                       /**< Pointer to the array of char contenaing the text to translate. */
	int textLength;                   /**< Length of the array of char  contenaing the text to translate. */

  /**
   * The two possible sates of the Arduino's pin defined as an enum class.
   */
	enum class PinState {low, high};
  /**
   * The five possible signs our class is able to deal with.
   * dot: a morse dot
   * dash: a morse dash
   * sp: a short pause
   * lp: a long pause
   * null: used in our char to signs mapping table
   */
	enum class Sign { dot, dash, sp, lp, null };
  

  /**
   * Mapping table between the lowercased alphabet [a-z] and their morse code equivalent.
   * It is a 26 rows by 5 columns table containing signs (from enum class Sign)
   */
	Sign map[26][5] = {
		{Sign::dot, Sign::dash, Sign::null, Sign::null, Sign::null}, //a
		{Sign::dash, Sign::dot, Sign::dot, Sign::dot, Sign::null}, //b
		{Sign::dash, Sign::dot, Sign::dash, Sign::dot, Sign::null}, //c
		{Sign::dash, Sign::dot, Sign::dot, Sign::null, Sign::null}, //d
		{Sign::dot, Sign::null, Sign::null, Sign::null, Sign::null}, //e
		{Sign::dot, Sign::dot, Sign::dash, Sign::dot, Sign::null}, //f
		{Sign::dash, Sign::dash, Sign::dot, Sign::null, Sign::null}, //g
		{Sign::dot, Sign::dot, Sign::dot, Sign::dot, Sign::null}, //h
		{Sign::dot, Sign::dot, Sign::null, Sign::null, Sign::null}, //i
		{Sign::dot, Sign::dash, Sign::dash, Sign::dash, Sign::null}, //j
		{Sign::dash, Sign::dot, Sign::dash, Sign::null, Sign::null}, //k
		{Sign::dot, Sign::dash, Sign::dot, Sign::dot, Sign::null}, //l
		{Sign::dash, Sign::dash, Sign::null, Sign::null, Sign::null}, //m
		{Sign::dash, Sign::dot, Sign::null, Sign::null, Sign::null}, //n
		{Sign::dash, Sign::dash, Sign::dash, Sign::null, Sign::null}, //o
		{Sign::dot, Sign::dash, Sign::dash, Sign::dot, Sign::null}, //p
		{Sign::dash, Sign::dash, Sign::dot, Sign::dash, Sign::null}, //q
		{Sign::dot, Sign::dash, Sign::dot, Sign::null, Sign::null}, //r
		{Sign::dot, Sign::dot, Sign::dot, Sign::null, Sign::null}, //s
		{Sign::dash, Sign::null, Sign::null, Sign::null, Sign::null}, //t
		{Sign::dot, Sign::dot, Sign::dash, Sign::null, Sign::null}, //u
		{Sign::dot, Sign::dot, Sign::dot, Sign::dash, Sign::null}, //v
		{Sign::dot, Sign::dash, Sign::dash, Sign::null, Sign::null}, //w
		{Sign::dash, Sign::dot, Sign::dot, Sign::dash, Sign::null}, //x
		{Sign::dash, Sign::dot, Sign::dash, Sign::dash, Sign::null}, //y
		{Sign::dash, Sign::dash, Sign::dot, Sign::dot, Sign::null} //z
		};

  /**
   * Array of Sign that will be filled with the translation of the input array of chars.
   */
	Sign signs[100];

	int pos;            /**< During the Arduino's main loop, position of the "cursor" in the signs array. */
	PinState pinState;  /**< During the Arduino's main loop, actual state of the pin. */
	unsigned long ts;   /**< Timestamp reseted at each pinState change. */

  /**
   * Function to trigger a new pin state
   * Used to avoid calling digitalWrite if not usefull.
   * Used to debug the class without an attached Arduino.
   */
	void updatePin(PinState newState);

public:
  /*
   * TODO: Use the following enum class to replace the array of char taken in input.
   *       At the moment, if the letter is not part of [a-z], it is ignored
   */
  //enum class Alphabet { a,b,c,d,e,f,g,h,i,j,k,l,m,n,o,p,q,r,s,t,u,v,w,x,y,z };

  /**
   * Constructor
   */
	Morse(int parameterPinNumber,     /**< Arduino's pin attached to a led*/
			unsigned long parameterDot,   /**< Duration of a dot in ms. */
			unsigned long parameterDash,  /**< Duration of a dash in ms. */
			unsigned long parameterWait,  /**< Duration between repetitions of the whole message. */
			char *parameterText,          /**< Pointer to the array of char contenaing the text to translate. */
			int parameterTextlength);     /**< Length of the array of char  contenaing the text to translate. */

  /**
   * Function to call on each Arduino's main loop.
   */
	void run();
};

#endif /* MORSE_H_ */
